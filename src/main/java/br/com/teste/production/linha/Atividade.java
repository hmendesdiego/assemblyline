package br.com.teste.production.linha;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Atividade {

    private String  tempo;

    private String descricao;

    public Atividade(String  tempo, String descricao){
        this.tempo = tempo;
        this.descricao = descricao;
    }

}
