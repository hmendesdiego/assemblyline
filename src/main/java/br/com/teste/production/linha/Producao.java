package br.com.teste.production.linha;

import br.com.teste.production.periodos.Periodo;
import br.com.teste.production.periodos.PeriodoManha;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Producao {

    private List<Atividade> linhaProducao;

    public void pegarAtividadesLinhaProducao() throws IOException {
//        try (Stream<String> stream = Files.lines(Paths.get("src\\main\\resources\\input.txt"))) {
//            this.linhaProducao = stream.map(str -> new Atividade(str, str)).collect(Collectors.toList());
//        }

        List<String> lines = Files.readAllLines(Paths.get("src\\main\\resources\\input.txt"));
        this.linhaProducao = lines.stream().map(line -> new Atividade(line, line)).collect(Collectors.toList());
        System.out.println(linhaProducao);
    }

    public void distribuirAtividades() {
        PeriodoManha manha = new PeriodoManha();
        adicionarAtividadesNoPeriodo(manha);


    }

    private void adicionarAtividadesNoPeriodo(Periodo periodo) {

    }
}
