package br.com.teste.production.main;

import br.com.teste.production.linha.Producao;

import java.io.IOException;

public class Init {

    public static void main(String[] args){

        try {
           Producao producao = new Producao();
           producao.pegarAtividadesLinhaProducao();
           producao.distribuirAtividades();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }



}
