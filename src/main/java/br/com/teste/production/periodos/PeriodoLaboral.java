package br.com.teste.production.periodos;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalTime;

@Getter
@Setter
public class PeriodoLaboral extends Periodo {

    private static final LocalTime JORNADA = LocalTime.of(1, 00);

    private static final LocalTime INICIO = LocalTime.of(16, 00);

    private static final LocalTime FINAL = LocalTime.of(17, 00);

    public PeriodoLaboral() {
        super(JORNADA, INICIO, FINAL);
    }
}
