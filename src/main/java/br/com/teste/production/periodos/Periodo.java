package br.com.teste.production.periodos;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public abstract class Periodo {

    private LocalTime jornada;

    private LocalTime inicio;

    private LocalTime fim;

    private List<String> atividades = new ArrayList<>();

    public Periodo(LocalTime jornada, LocalTime inicio, LocalTime fim) {
        this.jornada = jornada;
        this.inicio = inicio;
        this.fim = fim;
    }
}
