package br.com.teste.production.periodos;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalTime;

@Getter
@Setter
public class PeriodoTarde extends Periodo {

    private static final LocalTime JORNADA = LocalTime.of(5,00);

    private static final LocalTime INICIO = LocalTime.of(13, 00);

    private static final LocalTime FIM = LocalTime.of(17, 00);

    public PeriodoTarde() {
        super(JORNADA, INICIO, FIM);
    }
}
