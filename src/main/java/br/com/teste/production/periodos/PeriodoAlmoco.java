package br.com.teste.production.periodos;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalTime;

@Getter
@Setter
public class PeriodoAlmoco extends Periodo {

    private static final LocalTime JORNADA = LocalTime.of(1,00);

    private static final LocalTime INICIO = LocalTime.of(12, 00);

    private static final LocalTime FIM = LocalTime.of(13, 00);

    public PeriodoAlmoco() {
        super(JORNADA, INICIO, FIM);
    }
}
