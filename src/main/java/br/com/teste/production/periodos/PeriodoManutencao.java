package br.com.teste.production.periodos;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalTime;

@Getter
@Setter
public class PeriodoManutencao extends Periodo {

    private static final LocalTime JORNADA = LocalTime.of(0,5);

    public PeriodoManutencao() {
        super(JORNADA, null, null);
    }
}
