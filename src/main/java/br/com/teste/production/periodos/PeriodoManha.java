package br.com.teste.production.periodos;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalTime;

@Getter
@Setter
public class PeriodoManha extends Periodo {
    private static final LocalTime JORNADA = LocalTime.of(3,00);

    private static final LocalTime INICIO = LocalTime.of(9,00);

    private static final LocalTime FIM = LocalTime.of(12,00);

    public PeriodoManha() {
        super(JORNADA, INICIO, FIM);
    }
}
